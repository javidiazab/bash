#!/bin/bash

usu="C0261564"
pass="Javi0619"
urlPre3="https://api.sys.pre3.payhub.aws.santanderuk.pre.corp"
urlPre4="https://api.sys.pre4.payhub.aws.santanderuk.pre.corp"
preCi3="PayHub_pre-ci"
preCi4="PayHubGlobal_pre-ci"
grupo="payhub"

cfPre3() {
	a=`ccf login --skip-ssl-validation -u $usu -p $pass -a $urlPre3 -o $preCi3 -s $grupo`
	echo "Conexion establecida: $?"
}

cfPre4() {
	a=`cf login --skip-ssl-validation -u $usu -p $pass -a $urlPre3 -o $preCi4 -s $grupo`
	echo "Conexion establecida: $?"
}

push() {
	if [[ -f $2 ]]; then
		a=`cf push -f manifest.yml --var instances=1 -p ./build/libs/$S2` 
		echo "Push con manifest por parametros. No hace falta revertirlo: $?"
	else
		a=`cf push -f manifest.yml`
		echo "Push con manifest de proyecto. Hay que revertirlo: $?"
	fi
}

case $1 in
	3) cfPre3;;
	4) cfPre4;;
	push) push;;
	*) echo "Parametros invalidos. Usar: 3 / 4 / push / push <.jar>"
esac
