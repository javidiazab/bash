#!/bin/bash
#Limplia directorios java para dejar solo los src
#Boora los out, build y postman
#Ejemplo: /c/DATOS/WorkSpaceGitJavi/bash/pruebas/cleanJava.sh
rutaInicio=$1
rutaInicio=${rutaInicio:=.}  #asignamos valores por defecto

for dirname in $(ls -R $rutaInicio)
do
  echo "dirname: $dirname" # borra desde el final hasta que se encuentra el primer :
  #ruta=${dirname%:}
  if [[ "$dirname" == *out: ]] || [[ "$dirname" == *build: ]] || [[ "$dirname" == *postman: ]]  ; then
    echo "borrando $dirname"
    ruta=${dirname%:} # borra desde el final hasta que se encuentra el primer :
  #	`rm -rf "$ruta"`
  fi
  ruta=${dirname%:}
  if [[ -f $ruta ]] && [[ "$ruta" == *.hprof ]]; then
    echo "borrando fichero $ruta"
    #	`rm -rf "$ruta"`
  fi
done
