#!/bin/bash
#copia archivos previamente seleccionados para sacarlos
#Como no funciona al final se copian los ficheros a mano

fileArchivosACopiar=$1
rutaDestino=$2

fileArchivosACopiarDefecto="Archivos_a_procesar.txt"
fileArchivosACopiar=${fileArchivosACopiar:=$fileArchivosACopiarDefecto} #asignamos valores por defecto
rutaDestino=${rutaDestino:=./Backup}

crearRuta() {
	[ ! -e $1 ] && `mkdir $1`
}

copiarArchivos(){
	echo "Copiando archivos ..."
	crearRuta $rutaDestino

	#IFS=$'\n' # nuevo separador de campo, el caracter fin de línea
	backIFS=$IFS

	while IFS='' read -r linea || [[ -n "$linea" ]]; do
			echo "tratando: $linea"
			if [[ -d $linea ]]; then
			  echo "directorio: $linea"
			  echo "copia directorio $linea/ a $rutaDestino$linea/"
				`cp -r $linea/ $rutaDestino$linea/`
			elif [[ -f $linea ]]; then
			  echo "archivo: $linea"
				echo "copiar el fichero: $linea a $rutaDestino/${linea##*/}"
				`cp $linea $rutaDestino/${linea##*/}`
			else
			  echo "ni dir ni file: $linea"
			  `ls $linea`
			fi
	done < $fileArchivosACopiar

	IFS=$backIFS
}

#copiarArchivos

mkdir Backup
mkdir Backup/Ingles
cp '/c/DATOS/_Javi/z_Javi/Pendiente Javi.txt' ./Backup/
cp '/c/DATOS/_Javi/z_Javi/Varios.txt' ./Backup/
cp -r '/c/DATOS/_Javi/z_Javi/ISBAN/Cursos/201904 - Java' ./Backup/Cursos/
cp -r '/c/DATOS/_Javi/Bookmarks/' ./Backup/Bookmarks/
cp -r '/c/DATOS/_Javi/z_Javi/ISBAN/Ingles/' ./Backup/Ingles/
cp -r '/c/DATOS/_Javi/Prueba/' ./Backup/Prueba/
cp -r '/c/DATOS/_Javi/z_Javi/Manuales/' ./Backup/Manuales/


