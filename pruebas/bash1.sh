#!/bin/bash

echo""
echo "prueba0 ---------------------------------"
a="Hola"
b="mundo"
c="$a $b"
echo $c

echo""
echo "prueba1 ---------------------------------"
numero1=5
numero2=10
((resultado=numero1+numero2))
echo $resultado

echo""
echo "prueba2 ---------------------------------"
((resultado+=1))
echo $resultado

echo""
echo "prueba3 ---------------------------------"
lista=(Paquita1234 2 3)
echo ${#lista[0]}
echo ${lista[]}
echo ${#lista[*]}
# shellcheck disable=SC2068
echo ${lista[@]}
echo "El elemento ${lista[0]} mide ${#lista[0]} posiciones"

echo""
echo "prueba4 ---------------------------------"
current_dir=`pwd`
echo "The current directory is: $current_dir"
echo "The current day is: $(date)"

echo""
echo "prueba5 ---------------------------------"
var=echo `who`
echo $var

echo""
echo "prueba6 ---------------------------------"
output=$(/usr/bin/whoami)
echo $output


echo""
echo "prueba ficheros 1 -----------------------"

if [ -s /home/javi/BashScripting/readFile.sh ] # Si este fichero no está vacío...
then
	echo "readFile.sh tiene datos"
else
	echo "readFile.sh no tiene datos"
fi

if [ $? -ne 0 ] #Si el comando anterior falla el valor de $? no será 0, por lo tanto nos avisará con el siguiente mensaje
then
	echo "El comando anterior ha fallado: error code --> $?"
else
	echo "El comando anterior ha terminado bien --> $?"
fi


echo""
echo "prueba7 -----------------------"
#while (( i >= 1 )); do; echo “$i”; sleep 1; ((i—)); done

echo""
echo 'prueba8 -----------------------'
lista=(Alberto María Pepe Carla)
# shellcheck disable=SC2068
for i in ${lista[@]}
do
	echo $i
done


echo""
echo "prueba9 -----------------------"
valor="Si"
#case $1 in 
case $valor in
	si)
echo 'Has elegido que si'
	;;
	no)
echo 'Has elegido que no'
	;;
	*)
echo 'Eso no es una elecion'
	;;
esac

echo""
echo "prueba10 ----------------------"
x="readFile.sh"
echo `cat $x`

echo''
echo 'prueba11 ----------------------'
#fichero=”/home/javi/BashScripting/readFile.sh”
fichero='readFile.sh'
#palabras=ls $fichero
palabras=`cat $fichero`
#Crea una lista de las palabras del fichero
for palabra in $palabras
do
	echo $palabra
done

echo""
echo "prueba12 ----------------------"
echo 0{1..10}

echo""
echo "prueba12 ----------------------"
[[ abc =~ ^[a-z]$ ]] && echo si  #El primer valor es el string a comparar y el segundo es el regex el cual acepta expresiones regulares extendidas

echo""
echo "prueba12 ----------------------"
sTring=abc2de; echo ${sTring%2*}

echo""
echo "prueba12 ----------------------"
sTring=abcde; echo ${sString/ab/12}

echo""
echo "prueba12 ----------------------"
Arreglo[5]

echo""
echo "prueba12 ----------------------"
Arreglo=(1 2 3)
