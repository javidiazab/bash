#!/bin/bash
#Recoge todos los txt para pasarlos a un unico fichero txt con todo
rutaInicio=$1
rutaInicio=${rutaInicio:=.}  #asignamos valores por defecto

for dirname in $(ls -R $rutaInicio) do
  # shellcheck disable=SC2170
  if [[ -f $dirname ]] && [[ $dirname == *.txt ]]; then
    echo "Fichero: $dirname"
  fi
done
while IFS='' read -r linea || [[ -n "$linea" ]]; do
		#echo "echo1: $linea ${#linea}"
		#echo "echo2: ${linea:10:2}"
		if [[ $linea == *: ]]; then
			ruta=${linea%:} #borra desde el final hasta que se encuentra el primer :
			niveles=`grep -o "/" <<< $ruta | wc -l`    #busca los / y los pone en una linea cada uno luego cuenta lineas
			#echo "nivel: $niveles - $nivelesAMostrar - $ruta"
			if [[ $niveles -le $nivelesAMostrar ]]; then
				echo "Tratando $ruta"
				$(du -bsh "$ruta" | sed 's/\t/ /g' >> $tamanos)
			fi
		fi


done