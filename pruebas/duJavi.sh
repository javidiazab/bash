#!/bin/bash
#este script indica el tamano de cada directorio
#Ejemplo: /c/DATOS/WorkSpaceGitJavi/bash/pruebas/duJavi.sh 1 .
nivelesAMostrar=$1
nivelesAMostrar=${nivelesAMostrar:=1}  #asignamos valores por defecto
rutaInicio=$2
rutaInicio=${rutaInicio:=.}  #asignamos valores por defecto

FileRutas="Archivos_a_procesar.txt"
tamanos="tamanos.txt"
tamanosGrandes="tamanosGrandes.txt"

borrarFiles() {
	$(rm tama*.txt)
}

obtenerRutas(){
	echo "Obteniendo rutas ..."
	`ls -R $rutaInicio | sed '/^$/d' > $FileRutas` #quita las lineas en blanco
}

tamanoDirectorios(){
	echo "Obteniendo tamanos ..."
	borrarFiles
	
	#IFS=$'\n' # nuevo separador de campo, el caracter fin de l�nea
	backIFS=$IFS

	while IFS='' read -r linea || [[ -n "$linea" ]]; do
		#echo "echo1: $linea ${#linea}"
		#echo "echo2: ${linea:10:2}"
		if [[ $linea == *: ]]; then
			ruta=${linea%:} #borra desde el final hasta que se encuentra el primer :
			niveles=`grep -o "/" <<< $ruta | wc -l`    #busca los / y los pone en una linea cada uno luego cuenta lineas
			#echo "nivel: $niveles - $nivelesAMostrar - $ruta"
			if [[ $niveles -le $nivelesAMostrar ]]; then
				echo "Tratando $ruta"
				$(du -bsh "$ruta" | sed 's/\t/ /g' >> $tamanos)
			fi
		fi
	done < $FileRutas
	$(sort $tamanos | grep "G " >> $tamanosGrandes)
	$(cat $tamanosGrandes)
	echo "Proceso: Finalizado."

	IFS=$backIFS
}

if [ $# == 0 ]; then
	echo "Uso: duJava.sh <niveles de directorios a mostrar> <ruta inicial>"
fi

obtenerRutas
tamanoDirectorios