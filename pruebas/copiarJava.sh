#!/bin/bash

rutaInicio=$1
rutaDestino=$2
FileRutas="borrar_resultado.txt"

rutaInicio=${rutaInicio:=.}  #asignamos valores por defecto
rutaDestino=${rutaDestino:=./Backup}

obtenerRutas(){
	echo "Obteniendo rutas ..."
	`ls -R $rutaInicio | sed '/^$/d' > $FileRutas` #quita las lineas en blanco
}

crearRuta() {
	[ ! -e $1 ] && `mkdir $1`
}

copiarArchivos(){
	echo "Copiando archivos ..."
	crearRuta $rutaDestino
	
	#IFS=$'\n' # nuevo separador de campo, el caracter fin de línea
	backIFS=$IFS

	while IFS='' read -r linea || [[ -n "$linea" ]]; do
		#echo "echo1: $linea ${#linea}"
		#echo "echo2: ${linea:10:2}"
		if [[ $linea == *: ]]; then
			ruta=${linea%:} #borra desde el final hasta que se encuentra el primer :
			niveles=`grep -o "/" <<< $ruta | wc -l`    #busca los / y los pone en una linea cada uno luego cuenta lineas
			if [[ $niveles == 1 ]]; then
				echo "Tratando $ruta"
			fi
		else
			#echo "tratando: $ruta/$linea"
			if [[ -d "$ruta/$linea" ]]; then
				#echo "directorio: $linea" 
				if [[ $linea == *src ]]; then  
					#echo "copia directorio  $ruta/$linea a  $rutaDestino/${ruta#*/}/$linea" 
					#echo "$ruta/$linea/ $rutaDestino/${ruta#*/}/"
					`cp -r "$ruta/$linea/" "$rutaDestino/${ruta#*/}/"`
				fi
			else
				if [[ $niveles == 1 ]]; then
					crearRuta "$rutaDestino/${ruta#*/}"
					#echo "copiar el fichero: $ruta/$linea  a  $rutaDestino/${ruta#*/}/"
					#echo "$ruta/$linea $rutaDestino/${ruta#*/}/"
					if [[ $linea != java_pid*.hprof ]]   #estos archivos ocupan mucho 
						`cp -r "$ruta/$linea $rutaDestino/${ruta#*/}/"`
					fi
				fi
			fi
		fi	
	done < $FileRutas

	IFS=$backIFS
}

obtenerRutas
copiarArchivos